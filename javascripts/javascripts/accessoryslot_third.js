var money = "vault_eco_balance";
var moneyVal = PlaceholderAPI.static.setPlaceholders(
  BukkitPlayer,
  "%" + money + "%"
);
var powerRaw = PlaceholderAPI.static.setPlaceholders(
  BukkitPlayer,
  "%aureliumskills_power%"
);

function getPower() {
  var power = powerRaw / 9;
  return power.toFixed(1);
}

function hasKit() {
  if (moneyVal >= 2000000 && getPower() > 40) {
    return "true";
  }
}
hasKit();
