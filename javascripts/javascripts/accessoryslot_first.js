var money = "vault_eco_balance";
var moneyVal = PlaceholderAPI.static.setPlaceholders(
  BukkitPlayer,
  "%" + money + "%"
);
var powerRaw = PlaceholderAPI.static.setPlaceholders(
  BukkitPlayer,
  "%aureliumskills_power%"
);

function getPower() {
  var power = powerRaw / 9;
  return power.toFixed(1);
}

function hasKit() {
  if (moneyVal >= 10000 && getPower() > 10) {
    return "true";
  }
}

hasKit();
