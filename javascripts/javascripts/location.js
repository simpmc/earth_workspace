var world = "player_world";
var worldName = PlaceholderAPI.static.setPlaceholders(
  BukkitPlayer,
  "%" + world + "%"
);
var region = "worldguard_region_name";
var regionName = PlaceholderAPI.static.setPlaceholders(
  BukkitPlayer,
  "%" + region + "%"
);

function hasKit() {
  if (worldName == "spawn") {
    if (regionName == "shop") {
      return "&f௎ &6Tạp hoá";
    }
  }

  if (worldName == "spawn") {
    if (regionName == "blacksmith") {
      return "&fオ &#AA4A44Nhà thợ rèn";
    }
  }

  if (worldName == "spawn") {
    if (regionName == "prize_corner") {
      return "&f௑ &eGacha đi bạn ơi";
    }
  }

  if (worldName == "spawn") {
    if (regionName == "dungeon_portal") {
      return "&f௓ &dVết nứt không gian";
    }
  }

  if (worldName == "spawn") {
    if (regionName == "ah") {
      return "&fௐ &6Chợ đen";
    }
  }

  if (worldName == "spawn") {
    if (regionName == "library") {
      return "&f௒ &#5885AFThư viện";
    }
  }

  if (worldName == "spawn") {
    if (regionName == "hall_of_fame") {
      return "&f௏ &6Sảnh đường danh vọng";
    }
  }

  if (worldName == "spawn") {
    if (regionName == "jeweler") {
      return "&f఺ &#89CFF0Tiệm bán đớ";
    }
  }

  if (worldName == "spawn") {
    if (regionName == "alchemy_room") {
      return "&f౎ &#ffb2aeTiệm thuốc lắc";
    }
  }

  if (worldName == "spawn") {
    return "&f௉ &#89CFF0Spawn";
  }

  if (worldName == "world_nether") {
    return "&fோ &#EE4B2BNether";
  }

  if (worldName == "world") {
    return "&fொ &#50C878Overworld";
  }

  if (worldName == "world_the_end") {
    return "&fௌ &#753a97The End";
  }

  if (worldName.startsWith("coven_")) {
    return "&f் &#753a97The Veil";
  }

  return "&8 Đang định vị...";
}

hasKit();
